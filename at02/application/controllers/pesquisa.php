<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesquisa extends CI_Controller {

	public function index()
	{
		$this->load->view('include/cabecalho');
		$this->load->view('include/menu');
		$this->load->view('Pesquisa');
		$this->load->view('include/rodape');
	}
}