<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function index()
	{
		$this->load->view('include/cabecalho');
		$this->load->view('home_page');
		$this->load->view('include/rodape');
	}

}
