<?php

class Quotes extends CI_Controller{
    function __construct(){
        parent::__construct();
    }
    
    function index(){
        $this->subscribe();
    }
    
    function subscribe(){
        $this->load->view('content/site_content');
    }
    
    function send_message(){
        $message = $this->input->post("message");
        $user_id = $this->input->post("user_id");
        $content = array(
            "en" => "$message"
        );
        
        $fields = array(
            'app_id' => "3505c61c-d73e-49d2-b21a-7623eccf074f",
            'filters' => array(array("field" => "tag", "key" => "user_id", "relation" => "=", "value" => "$user_id")),
            'contents' => $content
        );
        
        $fields = json_encode($fields);
        print("\nJSON sent:\n");
        print("$fields");
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic NjU4NGE5ZWEtMjJkYS00ZGNkLWE1MmYtyTRmZmE2ZTMwYmQ4'));
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
}
    