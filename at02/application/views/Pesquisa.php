<div class="container">
    <div class="jumbotron">
        <div class="row">
          <div class="col-md-9">
            <h3>Pesquisa de preço</h3>
            <form method="get" action="Resultado">
                <div class="row">
                    <!-- Text input-->
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-label" for="date">Espaço necessário em disco</label>
                            <input id="date" name="espaco" type="text" placeholder="Espaço em GB" class="form-control input-md">
                        </div>
                    </div>
                    <!-- Select Basic -->
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-label" for="time">Linguagem de programação</label>
                            <select id="time" name="linguagem" class="form-control">
                                <option value="JAVA">Java</option>
                                <option value="c#">C#</option>
                                <option value="Ruby">Ruby</option>
                                <option value="Java Script">Java Script</option>
                                <option value="C">C</option>
                                <option value="Python">Python</option>
                                <option value="PHP">PHP</option>
                                <option value="Outras">Outras</option>
                            </select>  
                        </div>
                    </div>
                    <div class="col-md-2">
                    <br>
                        <button class="btn btn-success">OK</button>
                    </div>
                </form>
          </div>
      </div>
</div>
