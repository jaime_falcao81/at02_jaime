<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<div class="starter-template">
    <h1>OneSignal - Assinatura</h1>
    <p class="lead">Página de Assinatura</p>
</div>
<div class="contact-form">  
    <p class="notice error"><?= $this->session->flashdata('error_msg') ?></p><br/>
    <form id="ServiceRequest" action="<?= base_url() ?>quotes/send_message" method='post'>
        <div class="form-group">
            <label class="control-label">Mensagem:</label>
            <input type="text" name="message" class="form-control" placeholder="Insira sua mensagem" value="" >
        </div>
        <div class="form-group">
            <label class="control-label">Mensagem:</label>
            <input type="text" name="user_id" class="form-control" readonly value="4444" >
        </div>
        <div id='submit_button'>
            <input class="btn btn-success" type="submit" name="submit" value="Enviar"/>
        </div>
    </form>
</div>
    
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
            appId: "3505c61c-d73e-49d2-b21a-7623eccf074f",
            subdomainName: 'lp2at02',
            autoRegister: true,
            promptOptions: {
            actionMessage: "Gostaria de receber notificações do nosso site?.",
            acceptButtonText: "Sim",
            cancelButtonText: "Não, obrigado"
        }
        }]);        
    </script>
    <script>
        function subscribe() {
            OneSignal.push(["registerForPushNotifications"]);
            event.preventDefault();
        }
        function unsubscribe(){
            OneSignal.setSubscription(true);
        }
        var OneSignal = OneSignal || [];
        OneSignal.push(function() {
            OneSignal.on('subscriptionChange', function (isSubscribed) {
                console.log("O status de assinatura agora é:", isSubscribed);
                OneSignal.sendTag("user_id","4444", function(tagsSent){
                    console.log("Tags enviadas!");
                });
            });
            var isPushSupported = OneSignal.isPushNotificationsSupported();
            if (isPushSupported){
                OneSignal.isPushNotificationsEnabled().then(function(isEnabled){
                    if (isEnabled){
                        console.log("Notificações por Push habilitadas");
                    }
                    else{
                        OneSignal.showHttpPrompt();
                        console.log("Notificações por Push não estão habilitadas");
                    }
                });
            }
            else{
                console.log("Notificações por Push não suportadas");
            }
            });
    </script>

</html>