<div class="container">
    <h2>CLOUD PUB / SUB</h2>
    <p>Um serviço global para dados de transmissão e transmissão de dados em tempo real e confiáveis</p>
    <h3>EXPERIMENTE DE GRAÇA</h3>
    <p>Middleware escalável de mensagens<p>
    <p>Cloud Pub / Sub é um serviço de mensagens em tempo real totalmente gerenciado que permite enviar e receber mensagens entre aplicativos independentes. Você pode aproveitar a flexibilidade do Cloud Pub / Sub para desacoplar sistemas e componentes hospedados na Google Cloud Platform ou em outros lugares da Internet. Ao construir com a mesma tecnologia que o Google usa, o Cloud Pub / Sub é projetado para fornecer "pelo menos uma vez" entrega em baixa latência com escalabilidade sob demanda para 1 milhão de mensagens por segundo (e além).<p>

    <p>Middleware escalável de mensagens</p>
    <p>Conecte tudo com tudo</p>
    <p>Use o Cloud Pub / Sub para publicar e assinar dados de várias fontes e, em seguida, usar o Google Cloud Dataflow para entender seus dados, tudo em tempo real. Use o Cloud Pub / Sub para reduzir as dependências entre os componentes das aplicações distribuídas. Cloud Pub / Sub é a mesma tecnologia de mensagens utilizada por muitos dos aplicativos do Google, desde anúncios para o Gmail.</p>
    <p>
      Conecte tudo com tudo
Empurre e Puxe

Cloud Pub / Sub é projetado para integração rápida com sistemas hospedados na Google Cloud Platform ou em qualquer outro lugar, quer você precise de uma para uma, de um para muitos, ou de muitos para muitos, com entrega push ou pull.

Melhorar a tolerância a falhas
Entrega Garantida

Cloud Pub / Sub é projetado para fornecer "pelo menos uma vez" a entrega, armazenando cópias de mensagens em várias zonas para garantir que os assinantes possam receber mensagens o mais rápido possível. Todos os dados da mensagem são criptografados e protegidos no fio e em repouso.

Entrega Garantida
Global e escalável

    </p>
      <h3>Referência da API</h3>
      <p>
        A maneira mais fácil e recomendada para a maioria dos usuários usar a API do Google Cloud Pub / Sub é com nossas bibliotecas de clientes fornecidas. Se você é um usuário avançado e nossas bibliotecas de clientes não atendem suas necessidades específicas, você também pode desenvolver suas próprias bibliotecas de clientes para acessar diretamente as interfaces REST / HTTP e gRPC do Google Cloud Pub / Sub. Você encontrará todos os recursos relevantes nesta seção.
      </p>
      <h3>Bibliotecas de nuvem Pub / Sub Client</h3>
      <p>
        Esta página mostra como começar as novas bibliotecas do Cloud Client para a Google Cloud Pub / Sub API. No entanto, recomendamos usar as bibliotecas de clientes anteriores da API do Google se estiver executando no ambiente padrão do Google App Engine. Leia mais sobre as bibliotecas de clientes para APIs de nuvem em Bibliotecas de clientes explicadas.
        <h4>Alfa</h4>
      <p>
        Esta é uma versão Alpha das Bibliotecas do Cliente Cloud para a API Google Cloud Pub / Sub. Essas bibliotecas podem ser alteradas de forma incomparável e não são recomendadas para uso em produção. Eles não estão sujeitos a qualquer SLA ou política de depreciação.
      </p>
</div>

